//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Admin.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class ADM_GetJasaPaketMCU
    {
        public string MCUID { get; set; }
        public string JasaID { get; set; }
        public string KomponenID { get; set; }
        public string KomponenName { get; set; }
        public decimal Harga { get; set; }
        public Nullable<decimal> HargaHC { get; set; }
    }
}
