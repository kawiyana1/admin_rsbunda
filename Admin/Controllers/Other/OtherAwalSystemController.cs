﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Other;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Admin")]
    public class OtherAwalSystemController : Controller
    {
        string AddSpacesToSentence(string text, bool preserveAcronyms)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]))
                    if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                        (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                         i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                        newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var model = new List<OtherAwalSystemModel>();
                    foreach (var x in s.ADM_GetSetupAwal.OrderBy(x => x.Nomor).ToList()) 
                    {
                        model.Add(new OtherAwalSystemModel() { 
                            Nama = x.SetupName,
                            Display = AddSpacesToSentence(x.SetupName, true),
                            Nilai = x.Nilai,
                            AkunNo = x.Akun_No,
                            AkunNama = x.Akun_Name,
                            Tipe = x.Tipe
                        });
                    }
                    return View(model);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(List<OtherAwalSystemModel> model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        foreach (var x in model) {
                            s.ADM_InsertSetupAwal(x.Nama, x.Nilai ?? "");
                        };
                        s.SaveChanges();

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherAwalSystem;".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        #endregion
    }
}