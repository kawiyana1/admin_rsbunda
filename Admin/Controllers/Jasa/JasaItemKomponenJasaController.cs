﻿using Admin.Entities.SIM;
using Admin.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Admin.Models.Jasa;


namespace Admin.Controllers.Jasa
{
    public class JasaItemKomponenJasaController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListKomponenJasa.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.KomponenBiayaID.Contains(x.Value) || y.KomponenName.Contains(x.Value));

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        KomponenBiayaID = x.KomponenBiayaID,
                        KomponenName = x.KomponenName,
                        IncludeInsentif = x.IncludeInsentif,


                    }); ;
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, JasaItemKomponenJasaModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.ADM_InsertKomponenJasa(model.KomponenName, model.KelompokAkun, model.PostinganKe, model.IncludeInsentif, model.HutangKe, model.KelompokJasa, model.AkunNoHPP, model.AkunNoLawanHPP, model.ExcludeCostRS);

                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.ADM_UpdateKomponenJasa(model.KomponenBiayaID, model.KomponenName, model.KelompokAkun, model.PostinganKe, model.IncludeInsentif, model.HutangKe, model.KelompokJasa, model.AkunNoHPP, model.AkunNoLawanHPP, model.ExcludeCostRS);
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            s.ADM_DeleteKomponenJasa(model.KomponenBiayaID);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupItemKomponenJasa-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKomponenJasa.FirstOrDefault(x => x.KomponenBiayaID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            IncludeInsentif = m.IncludeInsentif,
                            KomponenBiayaID = m.KomponenBiayaID,
                            KomponenName = m.KomponenName,
                            AkunNoHPP = m.AkunNoHPP,
                            AkunNoHPPName = m.AkunNoHPPName,
                            AkunLawanHPP = m.AkunNoLawanHPP,
                            AkunNoRI = m.AkunNoRI,
                            AkunNoRIName = m.AkunNoRIName,
                            AkunNoRJ = m.AkunNoRJ,
                            AkunNoRJName = m.AkunNoRJName,
                            AKunNoUGD = m.AKunNoUGD,
                            AkunNoUGDName = m.AkunNoUGDName,
                            ExcludeCostRS = m.ExcludeCostRS,
                            HutangKe = m.HutangKe,
                            KelompokAkun = m.KelompokAkun,
                            KelompokJasa = m.KelompokJasa,
                            KomponenFixed = m.KomponenFixed,
                            NominalCost = m.NominalCost,
                            PersenCost = m.PersenCost,
                            PosisiPetugas = m.PosisiPetugas,
                            PostinganKe = m.PostinganKe,
                            UntukPetugas = m.UntukPetugas,
                           
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}