﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherDiskonModel
    {
        public string Id { get; set; }
        public string Nama { get; set; }
        public string NamaInternasional { get; set; }
        public string Akun { get; set; }
        public string DenganPetugas { get; set; }
        public bool DiskonTotal { get; set; }
        public bool DiskonTidakLangsung { get; set; }
        public bool DiskonGroupJasaCheck { get; set; }
        public int? DiskonGroupJasa { get; set; }
        public bool DiskonKomponenCheck { get; set; }
        public string DiskonKomponen { get; set; }
        public bool ExceptionJasa { get; set; }
        public List<OtherDiskonDetailModel> Detail { get; set; }
    }

    public class OtherDiskonDetailModel 
    {
        public string Id { get; set; }
    }
}