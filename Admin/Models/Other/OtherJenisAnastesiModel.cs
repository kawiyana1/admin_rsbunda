﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherJenisAnastesiModel
    {
        public string Id { get; set; }
        public string Nama { get; set; }
    }
}