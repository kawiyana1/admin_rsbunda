﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Vendor
{
    public class VendorKategoriVendorModel
    {
        public string Kode_Kategori { get; set; }
        public string Kategori_Name { get; set; }
        public string Akun_No { get; set; }
    }
}