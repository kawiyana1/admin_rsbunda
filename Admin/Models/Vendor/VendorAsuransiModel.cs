﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Vendor
{
    public class VendorAsuransiModel
    {
        public string Asuransi_ID { get; set; }
        public string Asuransi_Name { get; set; }
        public Nullable<double> MarkUp_Prosen { get; set; }
        public bool TermasukObat { get; set; }

        public List<VendorAsuransiDetailModel> Detail { get; set; }
    }

    public class VendorAsuransiDetailModel
    {
        public string JasaID { get; set; }
        public string JasaName { get; set; }
    }
}