﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaGapahModel
    {
        public string KodeGapah { get; set; }
        public string Deskripsi { get; set; }
        public string KodeGapahLama { get; set; }
        public string DeskripsiLama { get; set; }
        
    }
}