﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaJenisLukaModel
    {
        public string KodeLuka { get; set; }
        public string Deskripsi { get; set; }
    }
}