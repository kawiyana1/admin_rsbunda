﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaAlatModel
    {
        public string IDAlat { get; set; }
        public string NamaAlat { get; set; }
    }
}